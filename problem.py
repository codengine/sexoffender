

def count_frequency(word, *files):
    word_frequency = {}
    word_list = []

    for file in files:
        f = open(file, 'r')
        contents = f.readlines()
        for line in contents:
            word_list += line.strip().split()
        f.close()

    for each_word in word_list:
        each_word = each_word.strip()
        if word_frequency.get(each_word):
            word_frequency[each_word] += 1
        else:
            word_frequency[each_word] = 1

    return word_frequency.get(word, 0)


from collections import OrderedDict

def most_frequent(n, l, file_name):
    n_most_frequent_words = []

    word_list = []

    file = open(file_name, 'r')
    file_content = file.readlines()
    for line in file_content:
        line_words = line.strip().split()
        word_list += line_words
    file.close()

    temp_word_list = []

    for each_word in word_list:
        each_word = each_word.strip()

        if len(each_word) >= l:
            temp_word_list += [each_word]

    word_list = temp_word_list

    word_frequency_dict = {}
    for each_word in word_list:
        if word_frequency_dict.get(each_word):
            word_frequency_dict[each_word] += 1
        else:
            word_frequency_dict[each_word] = 1

    wfd_sorted_desc = sorted(word_frequency_dict.items(), key=lambda x: x[1], reverse=True)

    n_most_frequent_words_tuple = wfd_sorted_desc[:n]

    n_most_frequent_words = [each_tuple[0] for each_tuple in n_most_frequent_words_tuple]

    return n_most_frequent_words

    # wfd_sorted_desc = sorted(word_frequency_dict.items(), key=lambda x: x[1], reverse=True)
    #
    # def get_key(item):
    #     return item[1]
    #
    # get_key = lambda item: item[1]
    #
    # wfd_sorted_desc = sorted(word_frequency_dict.items(), key=get_key, reverse=True)


def most_frequent(n, l, *file_names):
    n_most_frequent_words = []

    word_list = []

    for file_name in file_names:
        file = open(file_name, 'r')
        file_content = file.readlines()
        for line in file_content:
            line_words = line.strip().split()
            word_list += line_words
        file.close()

    temp_word_list = []

    for each_word in word_list:
        each_word = each_word.strip()

        if len(each_word) >= l:
            temp_word_list += [each_word]

    word_list = temp_word_list

    word_frequency_dict = {}
    for each_word in word_list:
        if word_frequency_dict.get(each_word):
            word_frequency_dict[each_word] += 1
        else:
            word_frequency_dict[each_word] = 1

    wfd_sorted_desc = sorted(word_frequency_dict.items(), key=lambda x: x[1], reverse=True)

    n_most_frequent_words_tuple = wfd_sorted_desc[:n]

    n_most_frequent_words = [each_tuple[0] for each_tuple in n_most_frequent_words_tuple]

    return n_most_frequent_words






